class Book:
    def __init__(self, title, author, rating):
        self.title = title
        self.author = author
        self.rating = rating
        self.bestseller = False

    def show(self):
        print("Book: ", self.title)
        print("Author: ", self.author)
        print("Rating: ", self.rating)
        print("Bestseller: ", self.bestseller)
        print()

    def set_bestseller(self, new_val):
        self.bestseller = new_val
