Feature: Searching books on amazon
Scenario: Simple search
  Given website "https://www.amazon.com/"
  Then search for "Java" in "Books"
  Then collect search results into a collection
  Then open "https://www.amazon.com/Best-Sellers-Kindle-Store-Computer-Programming/zgbs/digital-text/156140011"
  Then compare titles in bestsellers with titles in book collection
  Then check if from the collection contains book "https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/ref=sr_1_3"