# -*- coding: utf-8 -*-
from behave import *
from selenium.webdriver.support.select import Select

from models.book import Book
from pages.amazon.amazonBestseller import AmazonBestsellers
from pages.amazon.amazonDetails import AmazonItemDetails
from pages.amazon.amazonMain import AmazonMainPage
from pages.amazon.amazonResult import AmazonSearchResults


@given('website "{url}"')
def step(context, url):
    context.browser.get(url)


@then('search for "{request}" in "{category}"')
def step(context, request, category):
    amazon_main_page = AmazonMainPage(context.browser)

    search_bar = amazon_main_page.get_search_bar()
    search_bar.clear()
    search_bar.send_keys(request)

    select_categories = Select(amazon_main_page.get_select())
    select_categories.select_by_visible_text(category)

    search_button = amazon_main_page.get_search_button()
    search_button.click()


@then("collect search results into a collection")
def step(context):
    amazon_search_results_page = AmazonSearchResults(context.browser)
    books = []
    for element in amazon_search_results_page.get_results():
        book = Book(
            amazon_search_results_page.get_title(element),
            amazon_search_results_page.get_author(element),
            amazon_search_results_page.get_rating(element)
        )
        books.append(book)

    context.book_collection = books
    for book in context.book_collection:
        book.show()


@then('open "{url}"')
def step(context, url):
    context.browser.get(url)


@then('compare titles in bestsellers with titles in book collection')
def step(context):
    amazon_bestsellers_page = AmazonBestsellers(context.browser)
    bestsellers_list = amazon_bestsellers_page.get_bestsellers()

    for bestseller in bestsellers_list:
        bestseller_title = bestseller.get_attribute("Title")
        print("bestseller_title: ", bestseller_title)
        try:
            for book in context.book_collection:
                if book.title.find(bestseller_title) > -1:
                    book.set_bestseller(True)
                    break
        except TypeError:
            continue


@then('check if from the collection contains book "{url}"')
def step(context, url):
    context.browser.get(url)
    amazon_book_page = AmazonItemDetails(context.browser)
    requested_book = amazon_book_page.get_book()
    search_result = False
    for book in context.book_collection:
        if book.title.find(requested_book.title) > -1:
            search_result = True
            break
    print("Search result: ", search_result)
