from selenium import webdriver


def before_all(context):
    context.browser = webdriver.Chrome("drivers/chromedriver.exe")
    context.book_collection = []


def after_all(context):
    context.browser.quit()
