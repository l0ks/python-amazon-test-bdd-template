from pages.basePage import BasePage


class AmazonSearchResults(BasePage):
    results = ".//div[contains(@class, 's-result-list s-search-results sg-row')]/div[contains(@class,'s-result-item')]"
    title = ".//h2[@class='a-size-mini a-spacing-none a-color-base s-line-clamp-2']"
    author = ".//div/span/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/div"
    rating = ".//a[@class='a-popover-trigger a-declarative']//span"

    def get_results(self):
        elements = self.driver.find_elements_by_xpath(self.results)
        return elements

    def get_title(self, web_element):
        if len(web_element.find_elements_by_xpath(self.title)) > 0:
            title = web_element.find_element_by_xpath(self.title).get_attribute("textContent")
            title = title.rstrip('\n')
            return title
            # return web_element.find_element_by_xpath(self.title).get_attribute("textContent")
        else:
            return "Title not available"

    def get_author(self, web_element):
        author_element_size = web_element.find_elements_by_xpath(self.author)
        if len(author_element_size) > 0:
            author_element = web_element.find_element_by_xpath(self.author)
            author = []
            for element in author_element.find_elements_by_xpath(".//span | .//a"):
                author.append(element.get_attribute("innerHTML"))
            author_full = ''.join(author)
            author_full = author_full.rstrip('\n')
            return author_full
            # return ''.join(author)
        else:
            return "Author not available"

    def get_rating(self, web_element):
        if len(web_element.find_elements_by_xpath(self.rating)) > 0:
            return web_element.find_element_by_xpath(self.rating).get_attribute("textContent")
        else:
            return "Rating not available"
