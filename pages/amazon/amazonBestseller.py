from pages.basePage import BasePage


class AmazonBestsellers(BasePage):
    title = ".//span/div/span/a/div"

    def get_bestsellers(self):
        elements = self.driver.find_elements_by_xpath(self.title)
        return elements
