from models.book import Book
from pages.basePage import BasePage


class AmazonItemDetails(BasePage):
    title = ".//span[@id='productTitle']"

    def get_book(self):
        return Book(
            self.driver.find_element_by_xpath(self.title).get_attribute("textContent"),
            "Author not available",
            "Rating not available"
        )
