from pages.basePage import BasePage


class AmazonMainPage(BasePage):
    search_bar = ".//input[@id='twotabsearchtextbox']"
    select = ".//select[@id='searchDropdownBox']"
    search_button = ".//div[@class='nav-search-submit nav-sprite']//input[@class='nav-input']"

    def get_search_bar(self):
        element = self.driver.find_element_by_xpath(self.search_bar)
        return element

    def get_select(self):
        element = self.driver.find_element_by_xpath(self.select)
        return element

    def get_search_button(self):
        element = self.driver.find_element_by_xpath(self.search_button)
        return element
